{
    'name': 'Custom Reports',
    'author': 'Ahmed Barakat',
    'version': '0.1',
    'depends': [
        'account', 'sale',
    ],
    'category': 'Reporting',
    'update_xml': [
        'wizard/custom_partner_balance_view.xml',
        'custom_reports_report.xml',
    ],
}