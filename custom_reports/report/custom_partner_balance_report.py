# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import time
from dateutil import parser
from report import report_sxw


class report_custom_partner_balance(report_sxw.rml_parse):
    _name = 'report.custom.partner.balance'

    def __init__(self, cr, uid, name, context=None):
        super(report_custom_partner_balance, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'get_previous_balance': self._get_pervious_balance,
            'get_balances': self._get_balances,
        })
        self.context = context

    def _get_pervious_balance(self, date_from, partner_ref, journal_filter_by, account_id):
        filter = []
        period_from_to = 'No Dates'
        year_from_date = parser.parse(date_from).year
        fiscal_year_id = self.pool.get('account.fiscalyear').search(self.cr, self.uid,
                                                                    [('name', 'ilike', year_from_date)],
                                                                    context=self.context)
        first_day_in_year = str(year_from_date) + '-1-1'
        if fiscal_year_id:
            period_from_to = 'Date From: ' + first_day_in_year + '\n To: ' + date_from

        account_move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        partner_ids = partner_pool.search(self.cr, self.uid, [('ref', '=', partner_ref)], context=self.context)
        if partner_ref and not account_id:
            # filter = [('partner_id', '=', partner_ids[0]), ('date', '<=', date_from), ('date', '>=', first_day_in_year)]
            filter = [('partner_id', '=', partner_ids[0]), ('date', '<=', date_from)]
        elif account_id and not partner_ref:
            filter = [('account_id', '=', account_id[0]), ('date', '<=', date_from)]
        elif account_id and partner_ref:
            filter = [('partner_id', '=', partner_ids[0]), ('account_id', '=', account_id[0]), ('date', '<=', date_from)]
        else:
            filter = []
        if journal_filter_by == 'all':
            domain = filter
            account_move_line_ids = account_move_line_pool.search(self.cr, self.uid, domain)
        elif journal_filter_by == 'posted':
            domain = filter + [('move_id.state', '=', 'posted')]
            account_move_line_ids = account_move_line_pool.search(self.cr, self.uid, domain)
        else:
            account_move_line_ids = []
        account_move_line_objs = account_move_line_pool.browse(self.cr, self.uid, account_move_line_ids,
                                                               context=self.context)
        total_debit = sum([move_line_obj.debit for move_line_obj in account_move_line_objs])
        total_credit = sum([move_line_obj.credit for move_line_obj in account_move_line_objs])
        balance = total_debit - total_credit
        return {'period_from_to': period_from_to, 'total_debit': total_debit, 'total_credit': total_credit,
                'balance': balance}

    def _get_balances(self, date_from, date_to, partner_ref, journal_filter_by, account_id):
        filter = []
        account_move_line_pool = self.pool.get('account.move.line')
        partner_pool = self.pool.get('res.partner')
        partner_ids = partner_pool.search(self.cr, self.uid, [('ref', '=', partner_ref)], context=self.context)
        if partner_ref and not account_id:
            filter = [('partner_id', '=', partner_ids[0]), ('date', '<=', date_to), ('date', '>=', date_from)]
        elif account_id[0] and not partner_ref:
            filter = [('account_id', '=', account_id[0]), ('date', '<=', date_to), ('date', '>=', date_from)]
        elif account_id[0] and partner_ref:
            filter = [('partner_id', '=', partner_ids[0]), ('account_id', '=', account_id[0])]
        else:
            filter = []
        if journal_filter_by == 'all':
            domain = filter
            account_move_line_ids = account_move_line_pool.search(self.cr, self.uid, domain, order='date')
        elif journal_filter_by == 'posted':
            domain = filter + [('move_id.state', '=', 'posted')]
            account_move_line_ids = account_move_line_pool.search(self.cr, self.uid, domain, order='date')
        else:
            account_move_line_ids = []
        account_move_line_objs = account_move_line_pool.browse(self.cr, self.uid, account_move_line_ids,
                                                               context=self.context)
        list_of_details = []
        for account_move_line_obj in account_move_line_objs:
            if list_of_details:
                balance = list_of_details[-1]['balance'] + account_move_line_obj.debit - account_move_line_obj.credit
            else:
                balance = self._get_pervious_balance(date_from, partner_ref, journal_filter_by, account_id)[
                              'balance'] + account_move_line_obj.debit - account_move_line_obj.credit
            details = {'date': account_move_line_obj.date, 'move_name': account_move_line_obj.move_id.name,
                 'journal_name': account_move_line_obj.journal_id.name,
                 'date_maturity': account_move_line_obj.date_maturity, 'debit': account_move_line_obj.debit,
                 'credit': account_move_line_obj.credit, 'balance': balance}
            list_of_details.append(details)

        return list_of_details


report_sxw.report_sxw('report.custom.partner.balance.report', 'res.partner',
                      'addons/custom_reports/report/custom_partner_balance_report.rml',
                      parser=report_custom_partner_balance, header=False)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
