# -*- coding: utf-8 -*-
##############################################################################
#
# OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time
from dateutil import parser


class custom_partner_balance_report(osv.osv_memory):
    # _inherit = "account.common.account.report"
    _name = 'custom.partner.balance.report'
    _description = 'Custom Partner Balance Report'

    def _get_fiscal_year_id(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        report_objs = self.browse(cr, uid, ids, context=context)
        account_fiscalyear_pool = self.pool.get('account.fiscalyear')
        for report_obj in report_objs:
            year_from_date = parser.parse(report_obj.date_from).year
            fiscalyear_id = account_fiscalyear_pool.search(cr, uid, [('name', 'ilike', year_from_date)], context=context)
            if fiscalyear_id:
                res[report_obj.id] = fiscalyear_id[0]
            else:
                raise Warning('Date from is not included in defined Fiscal Year!!')
        return res

    def onchange_date_from(self, cr, uid, ids, date_from, context=None):
        year_from_date = parser.parse(date_from).year
        fiscal_year_id = self.pool.get('account.fiscalyear').search(cr, uid, [('name', 'ilike', year_from_date)], context=context)
        if fiscal_year_id:
            return {'value': {'fiscal_year_id': fiscal_year_id[0]}}
        else:
            raise Warning('Date from is not included in defined Fiscal Year!!')


    _columns = {
        'fiscal_year_id': fields.function(_get_fiscal_year_id, string='Fiscal Year', type='many2one', relation='account.fiscalyear'),
        'date_from': fields.date('Start Date', required=True, ),
        'date_to': fields.date('End Date', required=True, ),
        'partner_ref': fields.char('Partner Reference', size=64, required=False, ),
        'journal_filter_by': fields.selection([('all', 'All'), ('posted', 'Posted Only')], 'Journal Filter',
                                              required=True, ),
        'account_id': fields.many2one('account.account', 'Account', ),
    }

    _defaults = {
        'journal_filter_by': 'all',
        'date_from': lambda *a: time.strftime('%Y-%m-%d'),
        'date_to': lambda *a: time.strftime('%Y-%m-%d'),
        'partner_ref': lambda self, cr, uid, c={}: self.pool.get('res.partner').browse(cr, uid, c['active_id'],
                                                                                       context=c).ref,
    }

    def print_report(self, cr, uid, ids, context=None):
        datas = {
            'ids': [],
            'active_ids': context['active_ids'],
            'model': 'res.partner',
            'form': self.read(cr, uid, ids)[0]
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'custom.partner.balance.report',
            'datas': datas,
        }

        # def _print_report(self, cr, uid, ids, data, context=None):
        # data = self.pre_print_report(cr, uid, ids, data, context=context)
        #     return {'type': 'ir.actions.report.xml', 'report_name': 'custom.partner.balance.report', 'datas': data}


custom_partner_balance_report()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
